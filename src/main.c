#include <stdio.h>

#include "mem.h"
#include "mem_internals.h"

int main() {

    size_t heap_size = 5000;
    printf("Initializing heap with size %zu\n", heap_size);
    void *heap= heap_init(heap_size);

    printf("\nSuccessful allocation with enough memory\n");
    void *block1 = _malloc(200);
    void *block2 = _malloc(100);
    void *block3 = _malloc(20);
    _malloc(30);
    debug_heap(stdout, heap);


    printf("\nDeleting one block of memory\n");
    _free(block2);
    debug_heap(stdout, heap);

    printf("\nDeleting two blocks of memory\n");
    _free(block3);
    _free(block1);
    debug_heap(stdout, heap);

    printf("\nNot enough memory. New region extends the old one\n");
    _malloc(4000);
    _malloc(1000);
    debug_heap(stdout, heap);

    return 0;
}
