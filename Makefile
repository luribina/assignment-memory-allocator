CFLAGS=--std=c17 -Wall -pedantic -Isrc/ -ggdb -Wextra -Werror -DDEBUG
BUILD_DIR=build
SRC_DIR=src
CC=gcc

TARGET = memory_allocator

SOURCES = $(shell find $(SRC_DIR) -name *.c)
OBJECTS = $(SOURCES:%.c=$(BUILD_DIR)/%.o)

all: dir $(BUILD_DIR)/$(TARGET)

dir:
	@mkdir -p $(BUILD_DIR)/$(SRC_DIR)

$(OBJECTS): $(BUILD_DIR)/%.o : %.c
	$(CC) -c $< -o $@ $(CFLAGS)

$(BUILD_DIR)/$(TARGET) : $(OBJECTS)
	$(CC) $^ -o $@

clean:
	rm -rf $(BUILD_DIR)

